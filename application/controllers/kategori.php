<?php
    class Kategori extends CI_Controller{
        public function ikan(){
            $data['ikan']=$this->model_kategori->data_ikan()->result();
            $this->load->view('templates/header');
            $this->load->view('templates/sidebar');
            $this->load->view('ikan',$data);
            $this->load->view('templates/footer');
        }
        public function makanan(){
            $data['makanan']=$this->model_kategori->data_makanan()->result();
            $this->load->view('templates/header');
            $this->load->view('templates/sidebar');
            $this->load->view('makanan',$data);
            $this->load->view('templates/footer');
        }
        public function perlengkapan(){
            $data['perlengkapan']=$this->model_kategori->data_perlengkapan()->result();
            $this->load->view('templates/header');
            $this->load->view('templates/sidebar');
            $this->load->view('perlengkapan',$data);
            $this->load->view('templates/footer');
        }
    }
?>