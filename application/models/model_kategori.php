<?php
    class Model_kategori extends CI_Model{
        public function data_ikan(){
            return $this->db->get_where('tb_barang',array('kategori'=>'ikan'));
        }
        public function data_makanan(){
            return $this->db->get_where('tb_barang',array('kategori'=>'makanan'));
        }
        public function data_perlengkapan(){
            return $this->db->get_where('tb_barang',array('kategori'=>'perlengkapan'));
        }
    }
?>